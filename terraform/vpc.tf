module "vpc" {
  source  = "terraform-google-modules/network/google"
  version = "~>2.1"

  project_id              = var.project_id
  network_name            = "net01"
  routing_mode            = "GLOBAL"
  auto_create_subnetworks = "false"

  subnets = [
    {
      subnet_name           = "api-sn"
      subnet_ip             = "10.20.3.0/26"
      subnet_region         = var.region
      subnet_private_access = "true"
      subnet_flow_logs      = "false"

    }

  secondary_ranges = {

    api-sn = [
      {
        range_name    = "api-pods-sn"
        ip_cidr_range = "10.21.0.0/18"
      },
      {
        range_name    = "api-services-sn"
        ip_cidr_range = "10.25.0.0/24"
      },
    ]

  routes = []
}