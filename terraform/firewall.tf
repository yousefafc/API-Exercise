resource "google_compute_firewall" "iap" {
  name    = "iap"
  network = module.vpc.network_self_link

  allow {
    protocol = "all"
  }

  source_ranges = [
    "35.235.240.0/20",
  ]

}