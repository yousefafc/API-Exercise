provider "google" {
  project = var.project_id
  region  = var.region
  version = "3.44"
}

provider "google-beta" {
  project = var.project_id
  region  = var.region
  version = "3.44"
}

terraform {
  required_version = "0.13.5"

  backend "gcs" {
    bucket = "${var.project_id}-tfstate" #create a bucket in the project called: $project_id-tfstate
    prefix = var.project_id
  }
}