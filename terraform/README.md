# GCP infrastructure

Terraform code for GKE

Fill "project_id" with a gcp project id

# Connecting to Cluster

```
gcloud container clusters get-credentials cluster-name

```

```
kubectl apply -f gke_config/deployment.yml
```