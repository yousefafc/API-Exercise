project_id     = "" #add project_id
region         = "europe-west1"
zones          = ["europe-west1-b", "europe-west1-c", "europe-west1-d"]


network    = "projects/${var.project_id}/global/networks/net01"
subnet     = "projects/${var.project_id}/regions/europe-west1/subnetworks/api-sn"
