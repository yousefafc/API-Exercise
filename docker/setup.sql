LOAD DATA LOCAL INFILE 'titanic.csv' 
INTO TABLE titanic 
FIELDS TERMINATED BY ',' 
ENCLOSED BY '"' 
IGNORE 1 ROWS;
(Survived,Pclass,Name,Sex,Age,Siblings/Spouse Aboard,Parents/Children Aboard,Fare);
